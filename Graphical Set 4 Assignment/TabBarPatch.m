//
//  TabBarPatch.m
//  Atttributor
//
//  Created by Admin on 6/22/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "TabBarPatch.h"

@implementation TabBarPatch

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    [tabBarController.view setNeedsLayout];
}

@end

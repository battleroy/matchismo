//
//  SetCardView.h
//  Graphical Set 4 Assignment
//
//  Created by Admin on 7/4/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetCardView : UIView

@property (nonatomic, strong) NSNumber *symbol;
@property (nonatomic, strong) NSNumber *color;
@property (nonatomic, strong) NSNumber *shading;
@property (nonatomic) NSUInteger number;

@property (nonatomic) BOOL chosen;

@end

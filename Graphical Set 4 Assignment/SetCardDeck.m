//
//  SetCardDeck.m
//  Set 3 Assignment
//
//  Created by Admin on 6/30/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "SetCardDeck.h"
#import "SetCard.h"

@implementation SetCardDeck

- (instancetype)init
{
    self = [super init];
    
    if(self) {
        for (NSNumber *symbol in [SetCard validSymbols]) {
            for (NSNumber *shading in [SetCard validShadings]) {
                for (NSNumber *color in [SetCard validColors]) {
                    for (int number = 1; number <= [SetCard maxNumber]; ++number) {
                        SetCard *card = [[SetCard alloc] init];
                        card.symbol = symbol;
                        card.shading = shading;
                        card.color = color;
                        card.number = number;
                        [self addCard:card];
                    }
                }
            }
        }
    }
    
    return self;
}

@end

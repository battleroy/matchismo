//
//  AppDelegate.m
//  Graphical Set 4 Assignment
//
//  Created by Admin on 7/4/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "AppDelegate.h"
#import "TabBarPatch.h"

@interface AppDelegate ()
@property (nonatomic, strong) TabBarPatch *patch;
@end

@implementation AppDelegate

- (TabBarPatch *)patch
{
    if(!_patch)
    {
        _patch = [[TabBarPatch alloc] init];
    }
    return _patch;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
    tabBarController.delegate = self.patch;
    return YES;
}

@end

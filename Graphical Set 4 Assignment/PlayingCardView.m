//
//  PlayingCardView.m
//  CardView
//
//  Created by Admin on 6/16/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "PlayingCardView.h"
#import "PlayingCardDeck.h"
#import "PlayingCard.h"


@implementation PlayingCardView

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.

#pragma mark - Properties

- (NSString *)rankAsString
{
    return @[@"?", @"A", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"J", @"Q", @"K"][self.rank];
}

- (void)setRank:(NSUInteger)rank
{
    _rank = rank;
    [self setNeedsDisplay];
}

- (void)setSuit:(NSString *)suit
{
    _suit = suit;
    [self setNeedsDisplay];
}

- (void)setFaceUp:(BOOL)faceUp
{
    if (_faceUp != faceUp)
    {
        [UIView transitionWithView:self duration:0.5 options:UIViewAnimationOptionTransitionFlipFromLeft animations:
         ^{
            [self setNeedsDisplay];
          } completion:nil];
    }
    _faceUp = faceUp;
    [self setNeedsDisplay];
}

#pragma mark - Drawing

- (CGFloat)cornerRadius
{
    return (self.bounds.size.width + self.bounds.size.height ) / 30.0;
}

- (CGFloat)cornerOffset
{
    return [self cornerRadius];
}

- (void)drawRect:(CGRect)rect
{
    UIBezierPath *roundedRect = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:[self cornerRadius]];
    
    [roundedRect addClip];
    roundedRect.lineWidth = 2.0;
    
    [[UIColor whiteColor] setFill];
    [roundedRect fill];
    [[UIColor blackColor] setStroke];
    [roundedRect stroke];
    
    [self drawCorners];
}

- (void)drawCorners
{
    if(self.faceUp)
    {
        NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
        paragraph.alignment = NSTextAlignmentCenter;
        
        UIFont *font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
        font = [font fontWithSize:font.pointSize * [self cornerOffset] * 0.1];
        
        NSAttributedString *cornerText = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@", [self rankAsString], self.suit] attributes:@{NSFontAttributeName : font, NSParagraphStyleAttributeName : paragraph}];
        [cornerText drawInRect:CGRectMake([self cornerOffset], [self cornerOffset], cornerText.size.width, cornerText.size.height)];
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextTranslateCTM(context, self.bounds.size.width, self.bounds.size.height);
        CGContextRotateCTM(context, M_PI);
        
        [cornerText drawInRect:CGRectMake([self cornerOffset], [self cornerOffset], cornerText.size.width, cornerText.size.height)];
    }
    else
    {
        UIImage *cardImage = [UIImage imageNamed:@"cardback"];
        [cardImage drawInRect:self.bounds];
    }
}

#pragma mark - Initialization

- (void)setup
{
    self.backgroundColor = nil;
    self.opaque = NO;
    self.contentMode = UIViewContentModeRedraw;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setup];
    }
    return self;
}

@end

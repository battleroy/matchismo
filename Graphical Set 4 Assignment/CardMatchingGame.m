//
//  CardMatchingGame.m
//  Matchismo 2 Assignment
//
//  Created by Admin on 6/26/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "CardMatchingGame.h"

@interface CardMatchingGame ()
@property (strong, nonatomic) NSMutableArray *cards; // of Cards
@property (strong, nonatomic) NSMutableArray *chosenCards; // of Cards
@property (strong, nonatomic) Deck *deck;
@property (nonatomic) NSUInteger matchCount;
@property (nonatomic, readwrite) NSInteger score;
@end

@implementation CardMatchingGame

- (NSMutableArray *)cards
{
    if(!_cards)
        _cards = [[NSMutableArray alloc] init];
    return _cards;
}

- (NSMutableArray *)chosenCards
{
    if(!_chosenCards)
        _chosenCards = [[NSMutableArray alloc] init];
    return _chosenCards;
}

- (instancetype)initWithCardCount:(NSUInteger)count usingDeck:(Deck *)deck
{
    return [self initWithCardCount:count matchCount:2 usingDeck:deck];
}

- (instancetype)initWithCardCount:(NSUInteger)count matchCount:(NSUInteger)matchCount usingDeck:(Deck *)deck
{
    self = [super init];
    
    if(self) {
        self.deck = deck;
        self.matchCount = (matchCount >= 2 && matchCount <= count) ? matchCount : 2;
        for(int i = 0; i < count; ++i) {
            Card *card = [deck drawRandomCard];
            if(card) {
                [self.cards addObject:card];
            } else {
                return nil;
            }
        }
    }
    
    return self;
}

- (Card *)cardAtIndex:(NSUInteger)index
{
    return (index < [self.cards count]) ? self.cards[index] : nil;
}

static const int MISMATCH_PENALTY = 2;
static const int COST_TO_CHOOSE = 1;

- (MatchResponse *)chooseCardAtIndex:(NSUInteger)index
{
    Card *card = [self cardAtIndex:index];
    MatchResponse *response = nil;
    
    if(!card.isMatched) {
        if(card.isChosen) {
            card.chosen = NO;
            [self.chosenCards removeObject:card];
        } else {
            [self.chosenCards addObject:card];
            self.score -= COST_TO_CHOOSE;
            card.chosen = YES;
            
            if ([self.chosenCards count] == self.matchCount) {
                response = [card matchWithDetailedResponse:[self.chosenCards subarrayWithRange:NSMakeRange(0, [self.chosenCards count] - 1)]];
                if(response.score) {
                    NSLog(@"___Match score: %d, cards: %@", response.score, self.chosenCards);
                    
                    self.score += response.score;
                    //card.matched = YES;
                    for (Card *chosenCard in self.chosenCards)
                        chosenCard.matched = YES;
                    //otherCard.matched = YES;
                } else {
                    response = [[MatchResponse alloc] initWithMatchingCards:nil score:MISMATCH_PENALTY];
                    self.score -= MISMATCH_PENALTY;
                    for (Card *chosenCard in self.chosenCards)
                        chosenCard.chosen = NO;
                    //otherCard.chosen = NO;
                }
                [self.chosenCards removeAllObjects];

            }
        }
    }
    
    return response;
}

- (Card *)addNewCard
{
    Card *card = [self.deck drawRandomCard];
    if (card)
        [self.cards addObject:card];
    return card;
}

@end

//
//  ViewController.m
//  Graphical Set 4 Assignment
//
//  Created by Admin on 7/4/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "CardGameViewController.h"
#import "PlayingCardView.h"
#import "PlayingCardDeck.h"
#import "PlayingCard.h"
#import "SetCardView.h"
#import "SetCard.h"
#import "Grid.h"

@interface CardGameViewController () <UIDynamicAnimatorDelegate>
@property (nonatomic, strong) Grid *grid;
@property (nonatomic, strong) NSArray *previousPositions; // of NSValue's of CGRect's
@property (nonatomic, strong) UIDynamicAnimator *dynamicAnimator;
@property (nonatomic) BOOL cardsAreCollected;
@end

@implementation CardGameViewController

static const int CARD_COUNT = 12;
static const float CARD_ASPECT_RATIO = 2.0 / 3.0;

- (UIDynamicAnimator *)dynamicAnimator
{
    if(!_dynamicAnimator) {
        _dynamicAnimator = [[UIDynamicAnimator alloc] initWithReferenceView:self.playingDeskView];
        _dynamicAnimator.delegate = self;
        
        UICollisionBehavior *boundaryCollisionBehavior = [[UICollisionBehavior alloc] init];
        boundaryCollisionBehavior.translatesReferenceBoundsIntoBoundary = YES;
        
        [_dynamicAnimator addBehavior:boundaryCollisionBehavior];
    }
    return _dynamicAnimator;
}

- (void)dynamicAnimatorDidPause:(UIDynamicAnimator *)animator
{
    [animator removeAllBehaviors];
}

- (Grid *)grid
{
    if(!_grid)
        _grid = [[Grid alloc] init];
    return _grid;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.playingDeskView.opaque = NO;
    self.playingDeskView.backgroundColor = nil;
    self.cardsAreCollected = NO;
    
    [self.playingDeskView addGestureRecognizer:[[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinch:)]];
    
    [self initializePlayingDesk];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self updatePlayingDeskLayout];
}

- (void)initializePlayingDesk
{
    self.grid.size = self.playingDeskView.bounds.size;
    self.grid.cellAspectRatio = CARD_ASPECT_RATIO;
    self.grid.minimumNumberOfCells = CARD_COUNT;
    
    if(self.grid.inputsAreValid) {
        for (NSUInteger i = 0; i < self.grid.rowCount; ++i) {
            for(NSUInteger j = 0; j < self.grid.columnCount; ++j) {
                CGRect cardFrame = [self.grid frameOfCellAtRow:i inColumn:j];
                NSUInteger viewIndexInCardViews = i * self.grid.columnCount + j;
                UIView *cardView = [self createCardViewInFrame:cardFrame
                                                      withCard:[self.game cardAtIndex:viewIndexInCardViews]];
                [self.cardViews addObject:cardView];
                [self.playingDeskView addSubview:cardView];
                if (viewIndexInCardViews == CARD_COUNT - 1)
                    return;
            }
        }
    }
}

- (void)updatePlayingDeskLayout
{
    if(self.cardsAreCollected)
    {
        NSLog(@"(%f, %f) : %fw %fh", self.playingDeskView.frame.origin.x, self.playingDeskView.frame.origin.y,
              self.playingDeskView.frame.size.width, self.playingDeskView.frame.size.height);
        [UIView animateWithDuration:0.5 animations:^{
             for(UIView *cardView in self.cardViews) {
                 CGRect rectToMoveTo =
                 CGRectMake(self.playingDeskView.bounds.size.width / 2 - cardView.bounds.size.width / 2,
                            self.playingDeskView.bounds.size.height / 2 - cardView.bounds.size.height / 2,
                            cardView.bounds.size.width,
                            cardView.bounds.size.height);
                 cardView.frame = rectToMoveTo;
             }
         } completion:nil];
    } else {
        self.grid.size = self.playingDeskView.bounds.size;
        self.grid.cellAspectRatio = CARD_ASPECT_RATIO;
        self.grid.minimumNumberOfCells = [self.cardViews count];
        
        if(self.grid.inputsAreValid) {
            for (NSUInteger i = 0; i < self.grid.rowCount; ++i) {
                for(NSUInteger j = 0; j < self.grid.columnCount; ++j) {
                    CGRect cardFrame = [self.grid frameOfCellAtRow:i inColumn:j];
                    NSUInteger viewIndexInCardViews = i * self.grid.columnCount + j;
                    
                    [UIView animateWithDuration:0.5 animations:^{
                         ((UIView *)self.cardViews[viewIndexInCardViews]).frame = cardFrame;
                     }];
                    
                    if (viewIndexInCardViews == [self.cardViews count] - 1)
                        return;
                    
                }
            }
        }
    }

}

- (UIView *)createCardViewInFrame:(CGRect)frame withCard:(Card *)card
{
    PlayingCardView *cardView = nil;
    
    if([card isKindOfClass:[PlayingCard class]])
    {
        PlayingCard *playingCard = (PlayingCard *)card;
        
        cardView = [[PlayingCardView alloc] initWithFrame:frame];
        [cardView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)]];
        [cardView addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)]];
        cardView.rank = playingCard.rank;
        cardView.suit = playingCard.suit;
        cardView.faceUp = NO;
    }
    
    return cardView;
}

- (NSMutableArray *)cardViews
{
    if(!_cardViews)
        _cardViews = [[NSMutableArray alloc] init];
    return _cardViews;
}

- (CardMatchingGame *)game
{
    if(!_game)
        _game = [self newGame];
    return _game;
}

- (CardMatchingGame *)newGame
{
    return [[CardMatchingGame alloc] initWithCardCount:CARD_COUNT
                                            matchCount:2
                                             usingDeck:[self createDeck]];
}

- (Deck *)createDeck
{
    return [[PlayingCardDeck alloc] init];
}

- (IBAction)redeal
{
    self.game = nil;
    self.cardsAreCollected = NO;
    
    [UIView animateWithDuration:0.5 animations: ^{
        for (UIView *cardView in self.cardViews)
            cardView.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self.cardViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self.cardViews removeAllObjects];
        
        [self initializePlayingDesk];
        [self updateUIWithMatchResponse:nil];
    }];
}

- (void)pinch:(UIPinchGestureRecognizer *)recognizer
{
    if (recognizer.scale < 1 &&
        recognizer.state == UIGestureRecognizerStateEnded &&
        !self.cardsAreCollected
        ) {
        [self collectCards];
        self.cardsAreCollected = YES;
        
    }
}

- (void)collectCards
{
    for(UIView *cardView in self.cardViews)
    {
        UISnapBehavior *snapBehavior = [[UISnapBehavior alloc] initWithItem:cardView
                                                                snapToPoint:CGPointMake(self.playingDeskView.bounds.origin.x + self.playingDeskView.bounds.size.width / 2,
                                                                                        self.playingDeskView.bounds.origin.y + self.playingDeskView.bounds.size.height / 2)];
        
        [self.dynamicAnimator addBehavior:snapBehavior];
    }
}

- (void)pan:(UIPanGestureRecognizer *)recognizer
{
    if (self.cardsAreCollected)
    {
        [self.dynamicAnimator removeAllBehaviors];
        [self panCardViews:recognizer];
    }
}

- (void)panCardViews:(UIPanGestureRecognizer *)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateChanged)
    {
        CGPoint gesturePoint = [recognizer locationInView:self.playingDeskView];
        UIView *testView = (UIView *)[self.cardViews firstObject];
        CGRect frameToMoveTo = CGRectZero;
        
        frameToMoveTo.size = testView.bounds.size;
        gesturePoint.x -= frameToMoveTo.size.width / 2;
        gesturePoint.y -= frameToMoveTo.size.height / 2;
        
        //___________Checking bounds for X coord_______________//
        if (gesturePoint.x < 0)
            frameToMoveTo.origin.x = 0;
        else if (gesturePoint.x > self.playingDeskView.frame.size.width - testView.frame.size.width)
            frameToMoveTo.origin.x = self.playingDeskView.frame.size.width - testView.frame.size.width;
        else
            frameToMoveTo.origin.x = gesturePoint.x;
        
        //___________Checking bounds for Y coord_______________//
        
        if (gesturePoint.y < 0)
            frameToMoveTo.origin.y = 0;
        else if (gesturePoint.y > self.playingDeskView.frame.size.height - testView.frame.size.height)
            frameToMoveTo.origin.y = self.playingDeskView.frame.size.height - testView.frame.size.height;
        else
            frameToMoveTo.origin.y = gesturePoint.y;
        
        for(UIView *cardView in self.cardViews)
            cardView.frame = frameToMoveTo;
    }
}

- (void)tap:(UITapGestureRecognizer *)recognizer
{
    if (self.cardsAreCollected) {
        self.cardsAreCollected = NO;
        [self.dynamicAnimator removeAllBehaviors];
        [self updatePlayingDeskLayout];
    } else {
        [self touchCardView:recognizer.view];
    }
}

- (void)touchCardView:(UIView *)sender
{
    NSUInteger chosenViewIndex = [self.cardViews indexOfObject:sender];
    MatchResponse *response = [self.game chooseCardAtIndex:chosenViewIndex];
    
    [self updateUIWithMatchResponse:response];
}

- (void)updateUIWithMatchResponse:(MatchResponse *)response
{
    for (UIView *cardView in self.cardViews)
    {
        NSUInteger cardViewIndex = [self.cardViews indexOfObject:cardView];
        Card *card = [self.game cardAtIndex:cardViewIndex];
        
        if ([cardView isKindOfClass:[PlayingCardView class]] && [card isKindOfClass:[PlayingCard class]]) {
            PlayingCardView *playingCardView = (PlayingCardView *)cardView;
            PlayingCard *playingCard = (PlayingCard *)card;
            
            playingCardView.rank = playingCard.rank;
            playingCardView.suit = playingCard.suit;
            playingCardView.faceUp = playingCard.isChosen;
            
            [playingCardView setNeedsDisplay];
            
            if(card.isMatched)
                [UIView animateWithDuration:0.5 animations:^{
                    cardView.alpha = 0.0;
                }];
        }
    }
    
    self.scoreLabel.text = [NSString stringWithFormat:@"Score: %ld", (long)self.game.score];
}


@end

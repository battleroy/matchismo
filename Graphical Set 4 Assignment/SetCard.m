//
//  SetCard.m
//  Set 3 Assignment
//
//  Created by Admin on 6/30/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "SetCard.h"

@implementation SetCard

static const int MATCHED_SCORE = 3;

- (NSString *)description
{
    return [NSString stringWithFormat:@"__SetCard instance__ Contents are: %@", self.contents];
}

- (int)match:(NSArray *)otherCards
{
    return (int)[self matchWithDetailedResponse:otherCards].score;
}

- (MatchResponse *)matchWithDetailedResponse:(NSArray *)otherCards
{
    MatchResponse *response = nil;
    
    NSArray *allCards = [otherCards arrayByAddingObject:self];
    if ([allCards count]) {
        for(unsigned i = 0; i < [allCards count]; ++i) {
            SetCard *firstCard = allCards[i];
            for (unsigned j = i + 1; j < [allCards count]; ++j) {
                SetCard *secondCard = allCards[j];
                if ([firstCard.color isEqualToNumber:secondCard.color] ||
                    [firstCard.symbol isEqualToNumber:secondCard.symbol] ||
                    [firstCard.shading isEqualToNumber:secondCard.shading] ||
                    firstCard.number == secondCard.number
                    )
                    return response;
            }
        }
    }
    
    response = [[MatchResponse alloc] initWithMatchingCards:allCards score:[allCards count] * MATCHED_SCORE];
    
    return response;
}

- (NSString *)contents
{
    return nil;
}

+ (NSArray *)validColors
{
    return @[@1, @2, @3];
}

+ (NSArray *)validShadings
{
    return @[@1, @2, @3];
}

+ (NSArray *)validSymbols
{
    return @[@1, @2, @3];
}

+ (NSUInteger)maxNumber
{
    return 3;
}

- (void)setSymbol:(NSNumber *)symbol
{
    for (NSNumber *otherSymbol in [SetCard validSymbols])
        if ([otherSymbol isEqualToNumber:symbol])
            _symbol = otherSymbol;
}

- (void)setShading:(NSNumber *)shading
{
    for (NSNumber *otherShading in [SetCard validShadings])
        if ([otherShading isEqualToNumber:shading])
            _shading = otherShading;
}

- (void)setColor:(NSNumber *)color
{
    for (NSNumber *otherColor in [SetCard validColors])
        if ([otherColor isEqualToNumber:color])
            _color = otherColor;
}

- (void)setNumber:(NSUInteger)number
{
    if(number >= 1 && number <= [SetCard maxNumber])
        _number = number;
}

@end

//
//  ViewController.h
//  Graphical Set 4 Assignment
//
//  Created by Admin on 7/4/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardMatchingGame.h"

@interface CardGameViewController : UIViewController

@property (strong, nonatomic) CardMatchingGame *game;
@property (strong, nonatomic) NSMutableArray *cardViews; // of CardViews
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UIView *playingDeskView;

- (void)tap:(UITapGestureRecognizer *)recognizer;
- (void)pan:(UIPanGestureRecognizer *)recognizer;
- (UIView *)createCardViewInFrame:(CGRect)frame withCard:(Card *)card;
- (CardMatchingGame *)newGame;
- (Deck *)createDeck;
- (void)updateUIWithMatchResponse:(MatchResponse *)response;
- (void)updatePlayingDeskLayout;

@end

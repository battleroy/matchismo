//
//  SetCardGameViewController.m
//  Graphical Set 4 Assignment
//
//  Created by Admin on 7/4/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "SetCardGameViewController.h"
#import "SetCardView.h"
#import "SetCard.h"
#import "SetCardDeck.h"
#import "SetCardMatchingGame.h"

@interface SetCardGameViewController ()

@end

@implementation SetCardGameViewController

static const int CARD_COUNT = 12;

- (UIView *)createCardViewInFrame:(CGRect)frame withCard:(Card *)card
{
    SetCardView *cardView = nil;
    
    if([card isKindOfClass:[SetCard class]]) {
        SetCard *setCard = (SetCard *)card;
        
        cardView = [[SetCardView alloc] initWithFrame:frame];
        [cardView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)]];
        [cardView addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)]];
        
        cardView.symbol = setCard.symbol;
        cardView.shading = setCard.shading;
        cardView.color = setCard.color;
        cardView.number = setCard.number;
        
        cardView.chosen = NO;
    }
    
    return cardView;
}

- (void)updateUIWithMatchResponse:(MatchResponse *)response
{
    for (UIView *cardView in self.cardViews) {
        NSUInteger cardViewIndex = [self.cardViews indexOfObject:cardView];
        Card *card = [self.game cardAtIndex:cardViewIndex];
        
        if ([cardView isKindOfClass:[SetCardView class]] && [card isKindOfClass:[SetCard class]]) {
            SetCardView *setCardView = (SetCardView *)cardView;
            SetCard *setCard = (SetCard *)card;
            
            setCardView.symbol = setCard.symbol;
            setCardView.shading = setCard.shading;
            setCardView.color = setCard.color;
            setCardView.number = setCard.number;
            setCardView.chosen = setCard.isChosen;
            
            [setCardView setNeedsDisplay];
            
            if(card.isMatched) {
                //[objectsToRemove addObject:cardView];
                [UIView animateWithDuration:0.5 animations:^{
                    cardView.alpha = 0.0;
                 }
                 completion:nil];
                //cardView.hidden = YES;
            }
        }
    }
    
    [super updatePlayingDeskLayout];
    
    self.scoreLabel.text = [NSString stringWithFormat:@"Score: %ld", (long)self.game.score];
}

- (IBAction)addNewCards:(UIButton *)sender
{
    static const int NEW_CARDS_COUNT = 3;
    for (int i = 0; i < NEW_CARDS_COUNT; ++i) {
        SetCard *card = (SetCard *)[self.game addNewCard];
        if(!card) {
            [super updatePlayingDeskLayout];
            [self showNoMoreCardsInDeckAlert];
            return;
        }
        
        UIView *cardView = [self createCardViewInFrame:CGRectZero withCard:card];
        [self.playingDeskView addSubview:cardView];
        [self.cardViews addObject:cardView];
    }
    
    [super updatePlayingDeskLayout];
}

- (void)showNoMoreCardsInDeckAlert
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops!"
                                                        message:@"There are no more cards in the deck."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}

- (CardMatchingGame *)newGame
{
    return [[SetCardMatchingGame alloc] initWithCardCount:CARD_COUNT
                                               matchCount:3 usingDeck:[self createDeck]];
}

- (Deck *)createDeck
{
    return [[SetCardDeck alloc] init];
}


@end

//
//  MatchResponse.m
//  Matchismo 2 Assignment
//
//  Created by Admin on 6/27/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "MatchResponse.h"

@interface MatchResponse ()
@property (nonatomic, strong, readwrite) NSArray *matchingCards;
@property (nonatomic, readwrite) NSUInteger score;
@end

@implementation MatchResponse

- (instancetype)init
{
    return [self initWithMatchingCards:nil score:0];
}

- (instancetype)initWithMatchingCards:(NSArray *)matchingCards score:(NSUInteger)score
{
    self = [super init];
    
    if(self) {
        self.matchingCards = matchingCards;
        self.score = score;
    }
    
    return self;
}

@end

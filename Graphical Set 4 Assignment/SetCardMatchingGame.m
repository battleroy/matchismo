//
//  SetCardMatchingGame.m
//  Set 3 Assignment
//
//  Created by Admin on 7/1/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "SetCardMatchingGame.h"

@implementation SetCardMatchingGame

- (instancetype)initWithCardCount:(NSUInteger)count usingDeck:(Deck *)deck
{
    return [self initWithCardCount:count matchCount:3 usingDeck:deck];
}

@end

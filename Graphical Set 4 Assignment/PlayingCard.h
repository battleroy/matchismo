//
//  PlayingCard.h
//  Matchismo 2 Assignment
//
//  Created by Admin on 6/26/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "Card.h"
#import "MatchResponse.h"

@interface PlayingCard : Card

@property (strong, nonatomic) NSString *suit;
@property (nonatomic) NSUInteger rank;

+ (NSArray *)validSuits; // of NSString
+ (NSUInteger)maxRank;

@end

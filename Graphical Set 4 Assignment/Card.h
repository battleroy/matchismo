//
//  Card.h
//  Matchismo 2 Assignment
//
//  Created by Admin on 6/26/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MatchResponse.h"

@interface Card : NSObject

@property (strong, nonatomic) NSString *contents;

@property (nonatomic, getter = isChosen) BOOL chosen;
@property (nonatomic, getter = isMatched) BOOL matched;

- (int)match:(NSArray *)otherCards;
- (MatchResponse *)matchWithDetailedResponse:(NSArray *)otherCards;

@end

//
//  SetCard.h
//  Set 3 Assignment
//
//  Created by Admin on 6/30/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "Card.h"

@interface SetCard : Card

@property (strong, nonatomic) NSNumber *symbol;
@property (strong, nonatomic) NSNumber *color;
@property (strong, nonatomic) NSNumber *shading;
@property (nonatomic) NSUInteger number;

+ (NSArray *)validSymbols;
+ (NSArray *)validColors;
+ (NSArray *)validShadings;
+ (NSUInteger)maxNumber;

@end

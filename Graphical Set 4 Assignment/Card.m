//
//  Card.m
//  Matchismo 2 Assignment
//
//  Created by Admin on 6/26/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "Card.h"

@implementation Card

- (MatchResponse *)matchWithDetailedResponse:(NSArray *)otherCards
{
    MatchResponse *response = nil;
    
    for(Card *card in otherCards)
        if([card.contents isEqualToString:self.contents])
            response = [[MatchResponse alloc] initWithMatchingCards:@[card] score:1];
    
    return response;
}

- (int)match:(NSArray *)otherCards
{
    int score = 0;
    
    for(Card *card in otherCards)
        if([card.contents isEqualToString:self.contents])
            score = 1;
    
    return score;
}

@end

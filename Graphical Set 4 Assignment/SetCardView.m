//
//  SetCardView.m
//  Graphical Set 4 Assignment
//
//  Created by Admin on 7/4/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "SetCardView.h"

@implementation SetCardView

#pragma mark - Properties

- (void)setSymbol:(NSNumber *)symbol
{
    _symbol = symbol;
    [self setNeedsDisplay];
}

- (void)setColor:(NSNumber *)color
{
    _color = color;
    [self setNeedsDisplay];
}

- (void)setShading:(NSNumber *)shading
{
    _shading = shading;
    [self setNeedsDisplay];
}

- (void)setNumber:(NSUInteger)number
{
    _number = number;
    [self setNeedsDisplay];
}

- (void)setChosen:(BOOL)chosen
{
    if (_chosen != chosen)
    {
        [UIView transitionWithView:self duration:0.2 options:UIViewAnimationOptionTransitionCrossDissolve animations:
         ^{
             [self setNeedsDisplay];
         } completion:nil];
    }

    _chosen = chosen;
    [self setNeedsDisplay];
}

#pragma mark - Drawing

- (CGFloat)cornerRadius
{
    return (self.bounds.size.width + self.bounds.size.height) / 30.0;
}

- (CGFloat)cornerOffset
{
    return [self cornerRadius];
}


- (void)drawRect:(CGRect)rect
{
    UIBezierPath *roundedRect = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:[self cornerRadius]];
    
    [roundedRect addClip];
    roundedRect.lineWidth = 2.0;
    
    if (self.chosen)
    {
        [[UIColor whiteColor] setFill];
    }
    else
    {
        [[UIColor grayColor] setFill];
    }
    [roundedRect fill];
    
    [[UIColor blackColor] setStroke];
    [roundedRect stroke];
    
    [self drawSymbols];
}

- (void)drawSymbols
{
    const CGFloat borderOffset = self.bounds.size.width / 5;
    const CGFloat symbolsOffset = self.bounds.size.height / 20;
    const CGFloat symbolWidth = self.bounds.size.width - 2 * borderOffset;
    const CGFloat symbolHeight = symbolWidth / 2;
    
    UIColor *symbolColor = nil;
    switch ([self.color intValue])
    {
        case 1:
            symbolColor = [UIColor redColor];
            break;
        case 2:
            symbolColor = [UIColor greenColor];
            break;
        case 3:
            symbolColor = [UIColor purpleColor];
            break;
        default:
            break;
    }
    
    [symbolColor setFill];
    [symbolColor setStroke];
    
    CGFloat y = (self.number % 2 == 0) ? (symbolHeight + symbolsOffset / 2) : symbolHeight / 2;
    for (NSInteger i = 0; i < self.number; i += 2, y += (symbolHeight + symbolsOffset))
    {
        CGRect rectToDrawSymbol1 = CGRectMake(borderOffset, self.bounds.size.height / 2 - y,
                                             symbolWidth, symbolHeight);
        CGRect rectToDrawSymbol2 = CGRectMake(borderOffset, self.bounds.size.height / 2 + y - symbolHeight,
                                              symbolWidth, symbolHeight);
        
        UIBezierPath *symbolPath1 = nil;
        UIBezierPath *symbolPath2 = nil;
        
        switch ([self.symbol intValue])
        {
            case 1:
                symbolPath1 = [self bezierPathForDiamondForRect:rectToDrawSymbol1];
                symbolPath2 = [self bezierPathForDiamondForRect:rectToDrawSymbol2];
                break;
            case 2:
                symbolPath1 = [self bezierPathForSquiggleForRect:rectToDrawSymbol1];
                symbolPath2 = [self bezierPathForSquiggleForRect:rectToDrawSymbol2];
                break;
            case 3:
                symbolPath1 = [self bezierPathForOvalForRect:rectToDrawSymbol1];
                symbolPath2 = [self bezierPathForOvalForRect:rectToDrawSymbol2];
            default:
                break;
        }
        
        [symbolPath1 stroke];
        if ([self.shading isEqualToNumber:@1])
        {
            [symbolPath1 fill];
        }
        else if ([self.shading isEqualToNumber:@2])
        {
            [self stripenBezierPath:symbolPath1];
        }
        
        if (self.number % 2 != 0 && i == 0)
        {
            i -= 1;
            continue;
        }
        
        [symbolPath2 stroke];
        if ([self.shading isEqualToNumber:@1])
        {
            [symbolPath2 fill];
        }
        else if ([self.shading isEqualToNumber:@2])
        {
            [self stripenBezierPath:symbolPath2];
        }
    }
}

- (void)stripenBezierPath:(UIBezierPath *)path
{
    CGContextSaveGState(UIGraphicsGetCurrentContext());
    
    [path addClip];
    
    for (CGFloat x = path.bounds.origin.x; x < path.bounds.origin.x + path.bounds.size.width; x += path.bounds.size.width / 5)
    {
        [path moveToPoint:CGPointMake(x, path.bounds.origin.y)];
        [path addLineToPoint:CGPointMake(x, path.bounds.origin.y + path.bounds.size.height)];
    }
    
    [path stroke];
    
    CGContextRestoreGState(UIGraphicsGetCurrentContext());
}

- (UIBezierPath *)bezierPathForSquiggleForRect:(CGRect)rect
{
    UIBezierPath *path = [[UIBezierPath alloc] init];
    
    const CGFloat frameWidthOffset = rect.size.width / 20;
    const CGFloat widthStep = (rect.size.width - 2 * frameWidthOffset) / 4;
    const CGFloat heightStep = rect.size.height / 4;
    
    [path moveToPoint:CGPointMake(rect.origin.x + frameWidthOffset, rect.origin.y + heightStep)];
    
    [path addCurveToPoint:CGPointMake(rect.origin.x + rect.size.width - frameWidthOffset, rect.origin.y + heightStep)
            controlPoint1:CGPointMake(rect.origin.x + frameWidthOffset + widthStep, rect.origin.y + 2 * heightStep)
            controlPoint2:CGPointMake(rect.origin.x + frameWidthOffset + 3 * widthStep, rect.origin.y)];
    [path addQuadCurveToPoint:CGPointMake(rect.origin.x + rect.size.width - frameWidthOffset, rect.origin.y + 3 * heightStep)
                 controlPoint:CGPointMake(rect.origin.x + rect.size.width, rect.origin.y + 2 * heightStep)];
    
    [path addCurveToPoint:CGPointMake(rect.origin.x + frameWidthOffset, rect.origin.y + 3 * heightStep)
            controlPoint1:CGPointMake(rect.origin.x + frameWidthOffset + 3 * widthStep, rect.origin.y + 2 * heightStep)
            controlPoint2:CGPointMake(rect.origin.x + frameWidthOffset + widthStep, rect.origin.y + 4 * heightStep)];
    [path addQuadCurveToPoint:CGPointMake(rect.origin.x + frameWidthOffset, rect.origin.y + heightStep)
                 controlPoint:CGPointMake(rect.origin.x, rect.origin.y + 3 * heightStep)];
    
    return path;
}

- (UIBezierPath *)bezierPathForDiamondForRect:(CGRect)rect
{
    UIBezierPath *path = [[UIBezierPath alloc] init];
    
    [path moveToPoint:CGPointMake(rect.origin.x, rect.origin.y + rect.size.height / 2)];
    
    [path addLineToPoint:CGPointMake(rect.origin.x + rect.size.width / 2, rect.origin.y)];
    [path addLineToPoint:CGPointMake(rect.origin.x + rect.size.width, rect.origin.y + rect.size.height / 2)];
    [path addLineToPoint:CGPointMake(rect.origin.x + rect.size.width / 2, rect.origin.y + rect.size.height)];
    [path closePath];
    
    return path;
}

- (UIBezierPath *)bezierPathForOvalForRect:(CGRect)rect
{
    UIBezierPath *path = [[UIBezierPath alloc] init];
    
    const CGFloat frameWidthOffset = rect.size.width / 7;
    
    [path moveToPoint:CGPointMake(rect.origin.x + frameWidthOffset, rect.origin.y)];
    
    [path addLineToPoint:CGPointMake(rect.origin.x + rect.size.width - frameWidthOffset, rect.origin.y)];
    [path addQuadCurveToPoint:CGPointMake(rect.origin.x + rect.size.width - frameWidthOffset, rect.origin.y + rect.size.height)
                 controlPoint:CGPointMake(rect.origin.x + rect.size.width, rect.origin.y + rect.size.height / 2)];
    [path addLineToPoint:CGPointMake(rect.origin.x + frameWidthOffset, rect.origin.y + rect.size.height)];
    [path addQuadCurveToPoint:CGPointMake(rect.origin.x + frameWidthOffset, rect.origin.y)
                 controlPoint:CGPointMake(rect.origin.x, rect.origin.y + rect.size.height / 2)];
    
    return path;
}


#pragma mark - Initialization

- (void)setup
{
    self.backgroundColor = nil;
    self.opaque = NO;
    self.contentMode = UIViewContentModeRedraw;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setup];
    }
    return self;
}

@end

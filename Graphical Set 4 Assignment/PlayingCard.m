//
//  PlayingCard.m
//  Matchismo 2 Assignment
//
//  Created by Admin on 6/26/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "PlayingCard.h"

@implementation PlayingCard

static const int RANKS_MATCHED_SCORE = 4;
static const int SUITS_MATCHED_SCORE = 1;

- (NSString *)description
{
    return [NSString stringWithFormat:@"__PlayingCard instance__ Contents are: %@", self.contents];
}

- (int)match:(NSArray *)otherCards
{
    return (int)[self matchWithDetailedResponse:otherCards].score;
}

- (MatchResponse *)matchWithDetailedResponse:(NSArray *)otherCards
{
    MatchResponse *response = nil;
    
    NSUInteger score = 0;
    
    if([otherCards count]) {
        /*_______ Checking for all RANKS matching ________*/
        
        BOOL allRanksMatched = YES;
        for (Card *otherCard in otherCards) {
            if (self.rank != ((PlayingCard *)otherCard).rank) {
                allRanksMatched = NO;
                break;
            }
        }
        if(allRanksMatched) {
            score = RANKS_MATCHED_SCORE * ([otherCards count] + 1);
            response = [[MatchResponse alloc] initWithMatchingCards:[otherCards arrayByAddingObject:self] score:score];
            return response;
        }
        
        /*_______ Checking for all SUITS matching ________*/
        
        BOOL allSuitsMatched = YES;
        for (Card *otherCard in otherCards) {
            if (![self.suit isEqualToString:((PlayingCard *)otherCard).suit]) {
                allSuitsMatched = NO;
                break;
            }
        }
        if(allSuitsMatched) {
            score =  SUITS_MATCHED_SCORE * ([otherCards count] + 1);
            response = [[MatchResponse alloc] initWithMatchingCards:[otherCards arrayByAddingObject:self] score:score];
            return response;
        }
        
        NSArray *allCards = [otherCards arrayByAddingObject:self]; // array with all cards to be checked (incl. self)
        
        /*_______ Checking for 2 of SUITS or RANKS matching ________*/
        
        int bestScore = 0;
        for (int i = 0; i < [allCards count]; ++i) {
            PlayingCard *firstCard = allCards[i];
            for (int j = i + 1; j < [allCards count]; ++j) {
                PlayingCard *secondCard = allCards[j];
                if (firstCard.rank == secondCard.rank && RANKS_MATCHED_SCORE > bestScore) {
                    bestScore = RANKS_MATCHED_SCORE * 2;
                    response = [[MatchResponse alloc] initWithMatchingCards:@[firstCard, secondCard] score:bestScore];
                } else if ([firstCard.suit isEqualToString:secondCard.suit] && SUITS_MATCHED_SCORE > bestScore) {
                    bestScore = SUITS_MATCHED_SCORE * 2;
                    response = [[MatchResponse alloc] initWithMatchingCards:@[firstCard, secondCard] score:bestScore];
                }
            }
        }
    }
    
    return response;
}

- (NSString *)contents
{
    return [[PlayingCard rankStrings][self.rank] stringByAppendingString:self.suit];
}

+ (NSArray *)validSuits
{
    return @[@"♥",@"♦",@"♠",@"♣"];
}

+ (NSArray *)rankStrings
{
    return @[@"?",@"A",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"J",@"Q",@"K"];
}

+ (NSUInteger)maxRank
{
    return [[PlayingCard rankStrings] count] - 1;
}

- (void)setRank:(NSUInteger)rank
{
    if (rank <= [PlayingCard maxRank])
        _rank = rank;
}

@synthesize suit = _suit;

- (void)setSuit:(NSString *)suit
{
    if([[PlayingCard validSuits] containsObject:suit])
        _suit = suit;
}

- (NSString *)suit
{
    return _suit ? _suit : @"?";
}

@end

//
//  CardMatchingGame.h
//  Matchismo 2 Assignment
//
//  Created by Admin on 6/26/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Deck.h"
#import "Card.h"

@interface CardMatchingGame : NSObject

- (instancetype)initWithCardCount:(NSUInteger)count matchCount:(NSUInteger)matchCount usingDeck:(Deck *)deck;
- (instancetype)initWithCardCount:(NSUInteger)count usingDeck:(Deck *)deck;

- (MatchResponse *)chooseCardAtIndex:(NSUInteger)index;
- (Card *)cardAtIndex:(NSUInteger)index;
- (Card *)addNewCard;

@property (nonatomic, readonly) NSInteger score;

@end

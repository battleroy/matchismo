//
//  MatchResponse.h
//  Matchismo 2 Assignment
//
//  Created by Admin on 6/27/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MatchResponse : NSObject

@property (nonatomic, strong, readonly) NSArray *matchingCards; // of Cards
@property (nonatomic, readonly) NSUInteger score;

- (instancetype)initWithMatchingCards:(NSArray *)matchingCards score:(NSUInteger)score;

@end
